package tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InvalidMoveException {
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();


            boolean invalidRow = false;
            int row, col = 0;
            do {
                System.out.print("Player " + player + " enter row number:");
                try {
                    int row = Integer.valueOf(reader.nextLine(reader.nextLine));
                    System.out.println("valid integer");
                    invalidRow = true;
                } catch (NumberFormatException ex) {
                    System.out.println("invalid row");
                    invalidRow = true;
                }
            }while (invalidRow);

            boolean invalidColomn = false;

            do {
                System.out.print("Player " + player + " enter row number:");
                try {
                    col = Integer.valueOf(reader.nextLine());
                    System.out.println("valid integer");
                    invalidColomn = true;

                } catch (NumberFormatException ex) {
                    System.out.println("invalid Integer");
                    invalidColomn = true;
                }
            }while (invalidColomn);
            try{
                board.move(row, col);
            }catch (InvalidMoveException e){
                System.out.println(e.getMessage());
            }
            System.out.println();
            board.move (row, col);
            System.out.println();


            System.out.print("Player " + player + " enter column number:");
            int col = Integer.valueOf(reader.nextLine());

            board.move(row, col);
            System.out.println(board);
        }


        reader.close();
    }


}
