public class Circle {

    static final double PI = 3.14;
    int radious;

    Point center;

    public Circle(int radious, Point center) {
        this.radious = radious;
        this.center = center;
    }

    public double area(){
        return PI * radious * radious;
    }
    public double perimeter(){
        return 2*PI * radious;

    }

    public boolean intersect (Circle circle){

        return radious + circle.radious >=
                center.distancePromPoint(circle.center);

    }
}
